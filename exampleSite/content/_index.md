---
title: "Neon-Learn theme for Hugo"
---


# Neon-Learn theme

[Hugo-theme-neon-learn](https://gitlab.com/dylanbstorey/neon-learn/) is a theme for [Hugo](https://gohugo.io/). This is an adaptation of the [Hugo-theme-learn](http://github.com/matcornic/hugo-theme-learn). It's designed for both documentation and "long form" articles for education. 

{{% notice tip %}}Learn theme works with a _page tree structure_ to organize content : All contents are pages, which belong to other pages. [read more about this]({{%relref "cont/pages/_index.md"%}}) 
{{% /notice %}}

## Main features

## Main features

* [Automatic Search]({{%relref "basics/configuration/_index.md#activate-search" %}})
* **Unlimited menu levels**
* **Automatic next/prev buttons to navigate through menu entries**
* [Image resizing, shadow...]({{%relref "cont/markdown.en.md#images" %}})
* [Attachments files]({{%relref "shortcodes/attachments.en.md" %}})
* [List child pages]({{%relref "shortcodes/children/_index.md" %}})
* [Mermaid diagram]({{%relref "shortcodes/mermaid.en.md" %}}) (flowchart, sequence, gantt)
* [Customizable look and feel and themes variants]({{%relref "basics/style-customization/_index.md"%}})
* [Buttons]({{%relref "shortcodes/button.en.md" %}}), [Tip/Note/Info/Warning boxes]({{%relref "shortcodes/notice.en.md" %}}), [Expand]({{%relref "shortcodes/expand.en.md" %}})