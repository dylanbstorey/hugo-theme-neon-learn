---
title: "Publish"
weight: 40
disableToc: true
---

# Publish 


## GitLab Pages

This will cause your 

1. create a .gitlab-ci.yml file in the root of your directory.
2. add the following lines : 
```yaml


variables:
  GIT_SUBMODULE_STRATEGY: recursive

test-pages:
  image: registry.gitlab.com/pages/hugo/hugo_extended
  stage: test
  script:
    - hugo -d test
  artifacts:
    paths:
    - test
  except:
    - main

pages:
  image: registry.gitlab.com/pages/hugo/hugo_extended
  stage: deploy
  script:
    - hugo
  artifacts:
    paths:
      - public/
  only:
    - main
```